### Omnivore
* Cat
* Dog
* Polar Bear
* Sheep
* Frog
* Rat
* Hawk
* Raccoon
* Chicken
* Fennec Fox

### Herbivore
* Horse
* Cow
* Capybara

### Carnivore
* Lion
* Fox
